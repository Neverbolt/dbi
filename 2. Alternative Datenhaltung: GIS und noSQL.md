# 2. Alternative Datenhaltung: GIS und noSQL (ZIM)

## NoSQL

Not Only SQL or NoSQL has recently gained traction due to the big-data craze. NoSQL databases usually don't have a rigidly defined schema and lack features of consistency, like foreign keys and others.

These tradeoffs let them handle much bigger data sets by letting them scale horizontally (using more machines), rather than vertically (using more powerful machines).

### CAP Theorem

Of course it would be great to be able to scale indefinitely and still have all the benefits that SQL systems provide, however the CAP theorem proves that out of **Consistency**, **Availability** and **Partition Tolerance** only two can ever be fully fulfilled at once.

* **Consistency**: All clients see the most recent data, updates are atomic on all nodes at once
* **Availability**: The data is always accessible, even if a node fails
* **Partition Tolerance**: The system still works even if the network between nodes is broken

From this stems the `ACID` principle for SQL databases which are usually `AC` systems from the CAP side.

* **Atomicity**: All or nothing
* **Consistency**: Internal consistency of data before and after all transactions
* **Isolation**: Changes are not visible until committed
* **Durability**: All committed changes are permanent even after crashes

The NoSQL crowd rather uses the `BASE` principle, which makes it fall in the `AP` and `CP` categories.

* **B**asically **A**vailable**: There is always data, though not always the most recent
* **S**oft state**: There is no guarantee for data permanency
* **E**ventual consistency**: State will be consistent, it just takes a while or two

Most NoSQL databases can be classified as one of four types:

* Key-Value stores: Unique keys with unstructured value (Redis)
* Column stores: Tables in 0th normal form with loads of NULLs (Cassandra)
* Graph databases: Nodes and paths (Neo4j)
* Document stores: Documents with whatever inside (mongoDB)

## MongoDB

With mongoDB data is stored in json-like documents that are grouped into collections. Most documents in a collection are similar to each other with common fields and common indexes, but there is no defined schema as in SQL tables.

The only thing that is the same in all documents is the `_id` field, which is an internal id, comparable to the `rowid` in Oracle.

The data can be queried in a JavaScript-esque way:

```sql
db.users.insert( {
  l_name: "sue",
  f_name: "mary",
  age: 15,
  likes: [ "stuff", "things" ]
} )

db.users.find( { age: { $lt: 18 } },  -- where
               { likes: 1, _id: 0 } ) -- columns (_id is present if not disabled)
        .sort( { age: 1 } )           -- sort (1 ASC, -1 DESC)
        .count()                      -- count(*)
        .distinct()                   -- distinct
        .limit(1)                     -- limit
        .skip(5)                      -- skip

db.users.delete( { age: { $gt: 18 } } )

db.users.update( { f_name: "mary" },                               -- where
                 { $set: { join_date: new Date() }, $unset: { } }, -- set
                 { multi: true } )                                 -- update multiple documents

db.users.createIndex( { f_name: 1 } )
```

Since the consistency guarantees for mongoDB are weakened to document level it is impossible to have reliable foreign keys. One could have a foreign key that points to another documents `_id`, however if an edit occurs it is not guaranteed that it will effect both the first as well as the foreign document.

One can however embed things in documents, though only to a limit of 16MB per document. This would guarantee consistency between the embedded documents, at the cost of the 0th normal form.

### Aggregation

A powerful feature of mongoDB is aggregation via its `aggregate` and `map-reduce` functionalities.

#### Aggregate

```sql
db.products.aggregate( [
  { $match: { type: "house" } },
  { $group: { _id: "$cust_id", total_cost: { $sum: "$cost" } }}
] )
```

Aggregate is the less powerful of the aggregation functions, but since it is native to mongoDB it is much faster.

#### Map-reduce

```sql
db.products.mapReduce(
  function() { emit( this.cust_id, this.cost ) },
  function(key, values) { return Array.sum( values ) },
  {
    query: { type: "house" },
    out: "total_cost"
  }
)
```

Map reduce is much more flexible and powerful than aggregate, since it can do its custom javascript processing, but it is much slower and with larger javascript functions it can be orders of magnitude slower.

## Geographic information systems

Geographic Information Systems (`GIS`) map, edit, organize, analyze and present spatial data. They consist of a GIS capable database, the spatial data itself and a GIS user interface. GIS databases are MsSQL, Oracle and Postgres, user interfaces are ArcGIS, GeoMedia, Microstation and QGIS.

Spatial data is always represented as latitudes and longitudes or xyz coordinates in a reference system, since the spherical earth can not be mapped 1:1 to a flat 2D shape. The most well known of those are the World Geodetic System (`WGS84`), the Gauss-Krüger projection, the Lambert-Projection and the UTM.

### Geometric objects

There are three major types of geometric objects that are used as spatial data which each have an `interior`, `boundary` and `exterior`:

|            |                                      Interior |                  Boundary |        Exterior |
| :--------- | --------------------------------------------: | ------------------------: | --------------: |
| Point      |                              the point itself |                        -- | everything else |
| Linestring | all points along the line other than the ends |         the two endpoints | everything else |
| Polygon    |             all points other than the outline | all points on the outline | everything else |

### Topological relations

Geometric objects have multiple relations to each other:

* A equals B: A and B share all the same points
* A is Disjoint to B: A and B share no same points
* A intersects B: A and B share interior points
* A touches B: A and B share only boundary points
* A crosses B: A is a linestring that goes through B but is not fully contained
* A is within B: A is contained in B, all interior points of A are in B
* A contains B: A is contained in B, all points of A are in B
* A overlaps B: A intersects B and A is the same geometry as B

### Spatial indexes

Spatial data can not be indexed like normal database data. It is rather separated into different layers of block sizes. This means that data is represented as four dimensional B-tree.
